package comvg.example.cursosandroid.micalculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtNum1;
    private EditText txtNum2;
    private TextView txtResul;
    private Button btnSumar, btnRestar, btnMultiplicar, btnDividir;
    private Button btnLimpiar, btnCerrar;

    private Operaciones op = new Operaciones(0.0f, 0.0f);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }

    public void initComponents() {
        txtNum1 = (EditText) findViewById(R.id.txtNum1);
        txtNum2 = (EditText) findViewById(R.id.txtNum2);
        txtResul = (TextView) findViewById(R.id.txtResul);

        btnRestar = (Button) findViewById(R.id.btnResta);
        btnSumar = (Button) findViewById(R.id.btnSuma);
        btnDividir = (Button) findViewById(R.id.btnDivi);
        btnMultiplicar = (Button) findViewById(R.id.btnMult);

        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        this.setEvents();

    }

    public void setEvents() {
        this.btnSumar.setOnClickListener(this);
        this.btnRestar.setOnClickListener(this);
        this.btnMultiplicar.setOnClickListener(this);
        this.btnDividir.setOnClickListener(this);
        this.btnLimpiar.setOnClickListener(this);
        this.btnCerrar.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        //Realizar operaciones

        switch (view.getId()) {

            case R.id.btnSuma: //sumar

                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResul.setText("" + op.suma());
                }
                break;
            case R.id.btnResta: //Restar

                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResul.setText("" + op.resta());
                }
                break;
            case R.id.btnMult: //Multiplicar

                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResul.setText("" + op.mult());
                }
                break;
            case R.id.btnDivi: //Dividir

                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    op.setNum1(Float.parseFloat(txtNum1.getText().toString()));
                    op.setNum2(Float.parseFloat(txtNum2.getText().toString()));
                    txtResul.setText("" + op.div());
                }
                break;
            case R.id.btnLimpiar: //Limpiar

                if(txtNum1.getText().toString().matches("") ||  txtNum2.getText().toString().matches("")){

                    Toast.makeText(MainActivity.this,
                            "Ingrese los datos solicitados: ", Toast.LENGTH_SHORT).show();}
                else {

                    txtNum1.setText("");
                    txtNum2.setText("");
                    txtResul.setText("");
                }
                break;
            case R.id.btnCerrar: //Cerrar
                    finish();

                break;




        }

    }
}


